# Model Management back end challenge

Please read the following instructions carefully.

**Your goal is to set up an application which enables access to a restricted API**

# Scenario

We want to give access to our internal API.

The user must be able to:

  - Register/login with an email address and password
  - Create multiple API tokens and see them in a list
  
Secondly:

  - Create an endpoint which is restricted, only accessible using a users token.
  - The endpoint should output in JSON all users email addresses and their tokens.

There is no requirement to code any front-end to the restricted endpoint. We can test this with Curl/Postman.

## Technology requirements

**Laravel** is a mandatory requirement. It's your choice whether to use a Laravel package to assist you or not.

Apart from this you can use any front-end framework, task runners and build processors to enhance the UX.

# Setup

- Download a copy of this repository
- Provide a a link to your finished solution on Github/Bitbucket
- Include a readme for all setup/installation instructions to run this locally

# Time limit

There is no hard time limit for this coding challenge, however we recommended spending max a few hours on this. We appreciate all the effort put into the challenge, we also do not want to take up too much of your time!

Good luck,
Model Management
